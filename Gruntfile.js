// JSHINT:
/*globals module, require */

// Source:
// http://gruntjs.com/getting-started

// Tips:
//  https://github.com/t32k/grunt-kss/issues/9
//    Error: Cannot find module './node_modules/grunt-kss/node_modules/kss/bin/kss-node'
//    Resolution: remove ./node_modules, remove 'kss' from ./package.json, npm install

// To run this task with Grunt:
//  cd PATH/TO/GRUNTFILE/DIR
//  grunt

// To run this task without Grunt:
//  cd PATH/TO/GRUNTFILE/DIR
//  node_modules/grunt-kss/node_modules/.bin/kss-node --config ct-kss-config.json

// This module contains utilities for handling and transforming file paths. Almost all these methods perform only string transformations. The file system is not consulted to check whether paths are valid.
var path = require('path');

// packpath helps you to find directories that contain package.json.
//var packpath = require('packpath');

module.exports = function(grunt) {

  "use strict"; // JSHINT - Use ECMAScript 5 Strict Mode

  var configDir = "../../"; // path.join(packpath.self(), 'config');
  var kss_project_config = grunt.file.readJSON( configDir + 'kss-project-config.json'); // grunt.config.data.kss_project_config

  kss_project_config.image_directory_styleguide_relative = path.relative( kss_project_config.destination, kss_project_config.ct_options.image_directory );

  // check whether .source is array and if so, use first element, otherwise use at is
  kss_project_config.style_dir_source = (kss_project_config.source.constructor === Array) ? kss_project_config.source[0] : kss_project_config.source;

  // JIT(Just In Time) plugin loader for Grunt.
  require('jit-grunt')(grunt, {
    // static mappings for plugin names that can not be resolved in the automatic mapping
    replace: 'grunt-text-replace'
  });

  // Display the elapsed execution time of grunt tasks
  require('time-grunt')(grunt);

  // NOTE! The <%= configDir %>ct-styleguide part of the path is also used in index.html to define where the search JSON is extracted from

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),// grunt.config.data.pkg
    kss_project_config: kss_project_config,
    configDir: configDir,
    styleguide_dir: (configDir + kss_project_config.destination),
    image_dir: (configDir + kss_project_config.ct_options.image_directory),
    style_dir: (configDir + kss_project_config.style_dir_source),
    tmp_dir: (configDir + kss_project_config.destination + '/tmp'),
    //path_to_root: "<%= configDir %>", // ./ <- node_modules <- ct__kss-node-template
    clean: {
      styleguide_pages: {
        options: {
          force: true // Warning: Cannot delete files outside the current working directory. Use --force to continue.
        },
        src: [
          // does there need to be a non-grunt fallback and how would that work?
          '<%= styleguide_dir %>/**/*.html' // note: filter to .html files if you have an old SVN project which puts .svn files in every directory
        ]
      },
      tipue_drop_data: {
        options: {
          force: true // Warning: Cannot delete files outside the current working directory. Use --force to continue. TODO: should not be reqd now
        },
        src: [
          '<%= tmp_dir %>/tipue_drop_data'
        ]
      }
    },
    // there is also grunt-newer-explicit, but that requires the 'dest' file to be specified, which is difficult with the styleguide
    // there is also grunt-compare-json (for the styleguide crops), but I'm not sure how to use that
    diff : {
      newer_css: {
        src : [
          '<%= style_dir %>/*.css'
        ],
        tasks : [
          'kss',
          //'execute:lwip',
          'exporter:styleguide_json'
        ]
      },
      newer_lwip_data: {
        src : [
          '<%= tmp_dir %>/styleguide_crops.json'
        ],
        tasks : [
          'execute:lwip'
        ]
      },
      newer_kss_project: {
        src : [
          '<%= configDir %>kss-project-config.json'
        ],
        tasks : [
          'merge-json:kss_config',
          'replace:styleguide_template'
        ]
      }
    },
    execute: {
      lwip: {
        options: {
              // like call but executed before/after looping the files
              before: function(grunt, options){
                  console.log('Cropping images...');
              },
              // argv=argument values
              // https://docs.nodejitsu.com/articles/command-line/how-to-parse-command-line-arguments
              args: [
                '--regenerate_all_images="<%= kss_project_config.ct_options.regenerate_all_images %>"',
                '--styleguide_directory="<%= styleguide_dir %>"',
                '--image_directory="<%= image_dir %>"'
              ]
          },
          src: ['lwip/app.js']
      }
    },
    exporter: {
      styleguide_json: {
        options: {
          silent: false, // note: setting this to true causes the generation to fail
          banner: ''
        },
        files: {
          src: '<%= styleguide_dir %>/*.html'
        }
      }
    },
    kss: {
      options: {
        template: '<%= configDir %>node_modules/ct__kss-node-template',
        config: '<%= configDir %>.kss-project-config.json'
      },
      dist: {
        // files object with paths comes from the .json config
      }
    },
    'merge-json': {
      kss_config: {
        options: {
          // http://stackoverflow.com/questions/4910567/json-stringify-how-to-exclude-certain-fields-from-the-json-string
          replacer: function(key, value) {

            // exclude our custom ct_options object from the merged kss-node config file
            if ( key === 'ct_options' ) {
              return undefined;
            }
            else {
              return value;
            }
          }
        },
        files: {
          '<%= configDir %>.kss-project-config.json': [ '<%= configDir %>kss-project-config.json', 'kss-template-config.json' ]
        }
      },
      tipue_drop_data: {
        files: {
          '<%= styleguide_dir %>/tipue_drop_data.json': [ '<%= tmp_dir %>/tipue_drop_data/**/*.json' ]
        }
      }
    },
    mkdir: {
      styleguide: {
        options: {
          create: [
            '<%= styleguide_dir %>' // else kss-node fails
          ]
        }
      }
    },
    replace: {
      "kss_whitespace_that_breaks_styleguide": {
        src: [
          '<%= style_dir %>/*.css'
        ],
        "overwrite": true,
        "replacements": [
          {
            // Scenario A:
            // <div class="foo">
            // <p>Bar</p>
            // </div>
            //
            //
            // Baz
            "from": /\n\n\n/gm,
            "to": "\n\n"
          },
          {
            // Scenario B:
            // <div class="foo">
            //
            // <p>div</p>
            // </div>
            "from": /\>\n\n.*</gm,
            "to": ">\n<"
          }
        ]
      },
      "styleguide_template": {
        "src": [
          "index.tpl.html"
        ],
        "dest": "index.html",
        "replacements": [
          {
            "from": /CONFIG_STYLEGUIDE_DIRECTORY/gm,
            "to": "<%= styleguide_dir %>"
          },
          {
            "from": /CONFIG_STYLEGUIDE_TITLE/gm,
            "to": "<%= kss_project_config.ct_options.styleguide_title %>"
          },
          {
            "from": /CONFIG_TEMP_DIRECTORY/gm,
            "to": "<%= tmp_dir %>"
          },
          {
            "from": /CONFIG_IMAGE_DIRECTORY_STYLEGUIDE_RELATIVE/gm,
            "to": "images" // <%= kss_project_config.image_directory_styleguide_relative %>"
          },
          {
            "from": /CONFIG_DESIGN_BREAKPOINT_END_MOBILE/gm,
            "to": "<%= kss_project_config.ct_options.design_breakpoint_end_mobile %>"
          },
          {
            "from": /CONFIG_DESIGN_BREAKPOINT_END_TABLET/gm,
            "to": "<%= kss_project_config.ct_options.design_breakpoint_end_tablet %>"
          },
          {
            "from": /CONFIG_LIVERELOAD_PORT/gm,
            "to": "<%= kss_project_config.ct_options.livereload_port %>"
          },
          {
            "from": /CONFIG_SHOW_WIREFRAMES/gm,
            "to": "<%= kss_project_config.ct_options.show_wireframes %>"
          },
          {
            "from": /CONFIG_SHOW_DESIGNS/gm,
            "to": "<%= kss_project_config.ct_options.show_designs %>"
          },
          {
            "from": /CONFIG_SHOW_DESIGN_AT_BREAKPOINT/gm,
            "to": "<%= kss_project_config.ct_options.show_design_at_breakpoint %>"
          },
          {
            "from": /CONFIG_SHOW_GRID_SHADING/gm,
            "to": "<%= kss_project_config.ct_options.show_grid_shading %>"
          },
          {
            "from": /CONFIG_SHOW_ALL_NAVIGATION_LEVELS/gm,
            "to": "<%= kss_project_config.ct_options.show_all_navigation_levels %>"
          },
          {
            "from": /CONFIG_KEEP_TARGET_IN_VIEWPORT/gm,
            "to": "<%= kss_project_config.ct_options.keep_target_in_viewport %>"
          }
        ]
      }
    }
  });

  grunt.registerTask("default", "Generate styleguide", function(target) {

    var tasks = {
      default: [ // without crops
        "mkdir:styleguide",
        "clean:styleguide_pages",
        "clean:tipue_drop_data", // disabled as we want to retain this data for diffing purposes
        "diff:flush", // TEMP
        "merge-json:kss_config",
        "replace:styleguide_template",
        "replace:kss_whitespace_that_breaks_styleguide",
        "kss",
        "exporter:styleguide_json",
        "merge-json:tipue_drop_data"
      ],
      crops: [ // with crops
        "mkdir:styleguide",
        "clean:styleguide_pages",
        "clean:tipue_drop_data", // disabled as we want to retain this data for diffing purposes
        "diff:flush", // TEMP
        "merge-json:kss_config",
        "replace:styleguide_template",
        "replace:kss_whitespace_that_breaks_styleguide",
        "kss",
        "exporter:styleguide_json",
        "merge-json:tipue_drop_data",
        "execute:lwip"
      ]
    };

    var do_crops = kss_project_config.ct_options.crop_images;

    grunt.task.run( do_crops ? tasks['crops'] : tasks['default'] );
  });

};