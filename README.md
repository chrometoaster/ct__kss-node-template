# CT__kss-node-template

Custom Chrometoaster template for KSS-node

## Special features

1. Styleguide search with autocomplete
1. Separate fields for wireframe image, and mobile, tablet and desktop designs
1. Component crops of full page designs
1. Grunt build

### 1. Styleguide search with autocomplete

Use `ct-tags` to specify search terms not present in the item title.

When JavaScript is enabled, entering a search term will result in a dropdown of matching results.

                /*
                Standard Button

                This is the standard button, which is usable with several focussable elements.

                ct-tags: control fancy

                .mod1 - modifier
                .mod1:hover - modifier hover state

                Styleguide buttons.standard
                */

### 2. Support for wireframe, mobile, tablet and desktop designs & notes

Note that this expects images to be available at `build/images`, wireframes to be in `.png` format and designs in `.jpg` format.

Notes are also supported, these may be in HTML format.

                /*
                Modal

                This is a styled popup.

                ct-wireframe: a_wireframe_export_from_uxpin.png

                ct-wireframe-notes: notes about the wireframe image.

                ct-design-desktop: a_desktop_design.jpg

                ct-design-desktop-notes: notes about the desktop image.

                ct-design-tablet: a_tablet_design.jpg

                ct-design-tablet-notes: notes about the tablet image.

                ct-design-mobile: a_mobile_design.jpg

                ct-design-mobile-notes: notes about the mobile image.

                .mod1 - modifier
                .mod1:hover - modifier hover state

                Styleguide modules.modal
                */

### 3. Component crops of full page designs

Crop syntax is `width,height,x_registration,y_registration`.

Note that this expects images to be available at `build/images`.

When JavaScript is enabled, a preview image will display showing the crop region in the context of the full page image.

                /*
                Modal

                This is a styled popup.

                ct-wireframe: a_wireframe_export_from_uxpin.png

                ct-wireframe-crop: 150,75,50,1000

                ct-design-desktop: a_desktop_design.jpg

                ct-design-desktop-crop: 200,100,250,0

                ct-design-tablet: a_tablet_design.jpg

                ct-design-tablet-crop: 200,100,100,0

                ct-design-mobile: a_mobile_design.jpg

                ct-design-mobile-crop: 200,100,0,0

                .mod1 - modifier
                .mod1:hover - modifier hover state

                Styleguide modules.modal
                */

### 4. Support for JavaScript setup triggers

This outputs the specified JavaScript with a button to run it.

                /*
                Modal

                This is a styled popup.

                ct-js: CT_UI.setup_modal()

                .mod1 - modifier
                .mod1:hover - modifier hover state

                Styleguide modules.modal
                */

### 5. Grunt build - TO UPDATE

The default Grunt task, `grunt`, performs the following steps:

1. Deletes any existing files in the target styleguide directory (workaround for [issue 181](https://github.com/kss-node/kss-node/issues/181))
1. Combines the variable `../kss-project-config.json` and the static `./kss-template-config.json`to create a single config file for KSS-node
1. Generates the styleguide into the target styleguide directory
1. Extracts JSON objects from the styleguide pages, merging these together to create `tipue_drop_data.json`, which powers the autocomplete searchbox
1. Deletes temporary files created during the process

#### Calling the plugin Grunt directly

        cd node_modules/ct__kss-node-template
        grunt

#### Calling the plugin Grunt from your project's Grunt file

Please refer to the partner repo: [ct__kss-node-demo](https://bitbucket.org/chrometoaster/ct__kss-node-demo)

#### Running the plugin without Grunt - TO FINISH

Please note that the search autocomplete will not return any results in this variation.

        # don't do this: kss-node --config ct-kss-config.json
        # The kss-node package name can't be used in the shell unless the package is installed globally
        # Source: http://blog.nodejs.org/2011/03/23/npm-1-0-global-vs-local-installation

        # do this instead
        # this redirects to node_modules/grunt-kss/node_modules/kss/bin/kss-node
        # you can check the version with node_modules/grunt-kss/node_modules/.bin/kss-node --version
        node_modules/grunt-kss/node_modules/.bin/kss-node --config node_modules/kss-node-ct-template/ct-kss-config.json

---

### Installation

        cd PATH/TO/PROJECT

        # This will install the template and its dependencies including bower dependencies (http://stackoverflow.com/a/18591690)
        # this is also why package.json uses 'dependencies'
        # rather than 'devDependencies' which requires installation from the template directory
        npm install git+https://bitbucket.org/chrometoaster/ct__kss-node-template.git --save-dev
        # if your project uses Grunt you should also install this
        npm install grunt-subgrunt --save-dev

You calso also pull the development `reusable_module` branch like so:

        npm install git+https://bitbucket.org/chrometoaster/ct__kss-node-template.git#reusable_module --save-dev

Note that the template expects to be installed to `./node_modules/ct__kss-node-template`.

#### Troubleshooting

##### npm install fails

1. Ensure that you have the latest version of Node.js installed
1. Clear the NPM cache: `sudo npm cache clean`

##### JSON generation fails

        Warning: Unable to parse "../../tmp/tipue_drop_data/section-N.json

and

        Warning: Unable to parse "../../tmp/tipue_drop_data/section-intro.json" file (Unexpected token /). Use --force to continue.

Avoid the scenario described in https://github.com/kss-node/kss-node/issues/197

* Use `named.indices`
* Avoid named indices with the same stub (https://github.com/kss-node/kss-node/issues/197):
        * `Styleguide intro` and `Styleguide intro-reveal` -> `Styleguide intro` and `Styleguide intro.reveal`
        * `Styleguide resource` and `Styleguide resources-and-downloads` -> `Styleguide resources-and-downloads.resource` and `Styleguide resources-and-downloads`

##### Styleguide generation fails

###### No compiled stylesheets

To resolve this, check that there are some uncompressed stylesheets (containing KSS comments) in the source directory.

###### Empty lines in Handlebars comments

To resolve this, replace empty lines with a dash (`-`):

        {{~!
                TYPE_NUMBER [boolean] - optional
                Determines whether to use the HTML5 number type or a regular text type,
                also used to determine the required message.
                eg: type_number=true
                -
                NUMBER_MESSAGE [string] - optional
                eg: number_message="Postcode must be a number"
        }}

###### Partial references a renamed styleguide index ([#210](https://github.com/kss-node/kss-node/issues/210)):

1. I reference a Handlebars partial in Styleguide index.subindex like so: {{> index.subindex}}
2. I then rename Styleguide index.subindex to Styleguide index.subindex1
3. The styleguide generation now fails

###### Missing closing tag in Handlebars code ([#210](https://github.com/kss-node/kss-node/issues/210)):

1. I make a mistake in my Handlebars code, eg I close an `{{#unless}}` with an `{{/if}}`
1. The styleguide generation now fails

###### Extra closing quote in Handlebars code

        Markup:
        {{> "modules.banner.image.template"
         heading="Heading text"
         heading_is_image=true
         intro="Intro text"
         image_include="img-1170x429--business.html"
         image_is_dark=true
         alert="Alert text"" -- HERE
         alert_url="/todo"
        }}

---

### Configuration options

Copy `kss-project-config-sample.json` to your project root as `kss-project-config.json` and customise the following as required.

#### source (String)

The path from the project `Gruntfile` to the folder containing your KSS-commented stylesheets

#### destination (String)

The path from the project `Gruntfile` to the folder where the styleguide will be output; note that this is currently restricted to 'ct-styleguide'

#### css (Array)

An array of paths to project-specific stylesheets required to display elements correctly within the styleguide, relative to the styleguide folder

#### js (Array)

An array of paths to project-specific scripts required to transform elements correctly within the styleguide, relative to the styleguide folder

#### ct_options

These options control Chrometoaster's enhancements

##### crop_images (Boolean)

Whether to perform processor-intensive image cropping (`true`) or not (`false`)

##### design_breakpoint_end_mobile (String)

The viewport width at which to hide any mobile designs shown in the styleguide

Requires: `crop_images:true`

##### design_breakpoint_end_tablet (String)

The viewport width at which to hide any tablet designs shown in the styleguide

Requires: `crop_images:true`

##### image_directory (String)

The path from the project `Gruntfile` to the folder containing designs, which are referenced in the styleguide

##### keep_target_in_viewport (Boolean)

Whether to scroll the target heading back into view after a resize (`true`) or not (`false`)

##### livereload_port (String or Boolean)

The port to use with the live reload server which refreshes the page assets when these are update as part of the build process.

To disable this, set to `false`.

##### regenerate_all_images (Boolean)

Whether to recreate all image crops (`true`) or only what is required (`false`).

##### show_all_navigation_levels (Boolean)

Whether to fully expand the styleguide navigation (`true`) or require the user to manually check a box to enable this (`false`)

##### show_designs (Boolean)

Whether to show design crops inside the styleguide (`true`) or require the user to manually check a box to see these (`false`)

Requires: `crop_images:true`

##### show_design_at_breakpoint (Boolean)

Whether to show mobile/tablet/desktop designs when the viewport is at the mobile/tablet/desktop width (`true`), or require the user to manually check a box to enable this (`false`)

Requires: `crop_images:true`

##### show_grid_shading (Boolean)

Whether to reveal grid outlines inside the styleguide (`true`) or require the user to manually check a box to see these (`false`)

##### show_wireframes (Boolean)

Whether to show wireframe crops inside the styleguide (`true`) or require the user to manually check a box to see these (`false`)

Requires: `crop_images:true`

##### styleguide_title (String)

The project name to display above the styleguide navigation

##### wireframe_src (String)

The name of the software used to generate wireframe screenshots