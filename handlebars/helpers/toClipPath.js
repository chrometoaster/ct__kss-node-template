/** @method
 * @name toClipPath
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Transforms a set of values into a CSS clip-path
 *
 * @param {String} crop_origin_x=0 - the x coordindate to start the crop at
 * @param {String} crop_origin_y=0 - the y coordindate to start the crop at
 * @param {String} crop_width=200 - the width of the crop
 * @param {String} crop_height=200 - the height of the crop
 *
 * @description
 This Handlebars helper for kss-node uses the 'helpers' path set in ct-kss-config.json.
 Inspiration: https://github.com/kss-node/kss-node/pull/117
 */

module.exports.register = function(handlebars) {
    handlebars.registerHelper('toClipPath', function(crop_origin_x, crop_origin_y, crop_width, crop_height) {

        var _crop_origin_x = parseInt(crop_origin_x, 10) || 0;
        var _crop_origin_y = parseInt(crop_origin_y, 10) || 0;
        var _crop_width = parseInt(crop_width, 10) || 200;
        var _crop_height = parseInt(crop_height, 10) || 200;

        var crop_tl = _crop_origin_x + "px " + _crop_origin_y + "px";
        var crop_tr = (_crop_origin_x + _crop_width) + "px " + _crop_origin_y + "px";
        var crop_br = (_crop_origin_x + _crop_width) + "px " + (_crop_origin_y + _crop_height) + "px";
        var crop_bl = _crop_origin_x + "px " + (_crop_origin_y + _crop_height) + "px";

        var crop = "";
            crop += crop_tl + ", ";
            crop += crop_tr + ", ";
            crop += crop_br + ", ";
            crop += crop_bl;

        return new handlebars.SafeString("style=\"-webkit-clip-path:polygon(" + crop + "); clip-path:polygon(" + crop + ");\"");
    });
};