/** @method
 * @name setJsTarget
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Used to provide a dynamic `#kss-ref` target to functions assigned to the `ct-js` custom variable.
 *
 * @param {String} ct_js - the functions to run when the 'Run the JavaScript below' button is clicked, containing placeholders of `#TARGET`
 * @param {String} referenceURI - the handlebars identifier of the parent KSS section
 *
 * @returns {String} ct_js - the same functions, targetting the parent KSS section rather than TARGET
 *
 * @description
 This Handlebars helper for kss-node uses the 'helpers' path set in ct-kss-config.json.
 Inspiration: https://github.com/kss-node/kss-node/pull/117
 *
 * @example
// in module.scss
ct-js:
CT_UI.setup_mobile_banner('#TARGET');
CT_UI.wrap_words('#TARGET h1', 3);
 */

module.exports.register = function(handlebars) {
    handlebars.registerHelper('setJsTarget', function(ct_js, referenceURI) {

        var referenceURI_toAnchor = '#kssref-' + referenceURI.replace(/\./g, "_").replace(/-/g, "_").replace(/,/g, "_");

        if ( ct_js.match( /#TARGET/ ) ) {

            ct_js = ( ct_js.replace( /#TARGET/g, referenceURI_toAnchor ) );

            //console.log('setJsTarget', referenceURI_toAnchor, ct_js);
        }

        return new handlebars.SafeString( ct_js );
    });
};