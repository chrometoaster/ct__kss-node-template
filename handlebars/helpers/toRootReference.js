/** @method
 * @name toRootReference
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Transforms a non-root styleguide reference (aka index) into a root one, eg buttons.standard -> buttons
 *
 * @param {String} reference - A styleguide reference (aka index)
 *
 * @description
 This Handlebars helper for kss-node uses the 'helpers' path set in ct-kss-config.json.
 Inspiration: https://github.com/kss-node/kss-node/pull/117
 */

module.exports.register = function(handlebars) {
    handlebars.registerHelper('toRootReference', function(reference) {
        return new handlebars.SafeString(reference.split('.')[0]);
    });
};