/** @method
 * @name hasMarkup
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Outputs a message if a Markup heading was used but there was no markup
 *
 * @param {String} markup - The markup string
 *
 * @description
 This Handlebars helper for kss-node uses the 'helpers' path set in ct-kss-config.json.
 Inspiration: https://github.com/kss-node/kss-node/pull/117
 */

/*
TODO: doesn't work as expected
module.exports.register = function(handlebars) {
    handlebars.registerHelper('hasMarkup', function(markup) {
        if ( ! markup.length ) {
            return "TO BE DEVELOPED!";
        }
    });
};
*/