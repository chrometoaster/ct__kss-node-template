/** @method
 * @name toAnchor
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Transforms a styleguide reference (aka index) containing illegal characters into one containing underscores, so that anchors function properly
 *
 * @param {String} referenceURI - the specified styleguide reference (aka index), eg form_patterns.modal
 *
 * @description
 This Handlebars helper for kss-node uses the 'helpers' path set in ct-kss-config.json.
 Inspiration: https://github.com/kss-node/kss-node/pull/117
 */

module.exports.register = function(handlebars) {
    handlebars.registerHelper('toAnchor', function(referenceURI) {
        return new handlebars.SafeString(referenceURI.replace(/\./g, "_").replace(/-/g, "_").replace(/,/g, "_"));
    });
};