/** @method
 * @name isCtLayout_Main
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Evaluates whether a particular layout has been specified, to reduce the number of custom properties
 *
 * @returns boolean
 *
 * @param {String} ct_layout - The custom ct_layout property
 *
 * @description

        {{isCtLayout_Main ct_layout}}
 */

 // TODO: not working

module.exports.register = function(handlebars) {
    handlebars.registerHelper('isCtLayout_Main', function() {
        return ( ct_layout === 'main' ) ? options.fn(this) : options.inverse(this)
    });
};