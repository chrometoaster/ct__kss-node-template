/** @method
 * @name getFileExtension
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Extract the file extension from a file name
 *
 * @param {String} fileName - the file name
 *
 * @returns {String} - eg 'pdf'
 *
 * @description
 This Handlebars helper for kss-node uses the 'helpers' path set in ct-kss-config.json.
 Inspiration: https://github.com/kss-node/kss-node/pull/117
 */

module.exports.register = function(handlebars) {
    handlebars.registerHelper('getFileExtension', function(fileName) {
        return new handlebars.SafeString( fileName.substr( fileName.lastIndexOf('.') + 1 ) );
    });
};