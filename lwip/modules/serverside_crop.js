// JSHINT:
/*globals require, module */

/** @function
 * @name serverside_crop
 * @requires fs
 * @requires glob
 * @requires is-there
 * @requires jsonfile
 * @requires crypto
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Crops a full page design image to the component area
 * @description Outputs a crop image and passes arguments to a front-end function which shows the crop area on the full page design
 *
 * @param {String} json_directory - The path to the JSON directory containing crop data (exported from the KSS styleguide)
 */

// constructor
var serverside_crop = function(json_directory) {

    // DEPENDENCIES

    // File System - File I/O is provided by simple wrappers around standard POSIX functions
    var fs = require('fs');

    // glob functionality for node.js
    var glob = require('glob');

    // A library to check if a file or directory exists in a given path
    var IsThere = require('is-there');

    // Easily read/write JSON files.
    var jf = require('jsonfile');

    // JavaScript implementations of standard and secure cryptographic algorithms
    var crypto = require('crypto');

    // GLOBALS

    var image_src_dir = require('../app').image_src_dir;
    var image_dest_dir = require('../app').image_dest_dir;
    var regenerate_all_images = require('../app').regenerate_all_images;
    var serverside_crop_image_async = require('./serverside_crop_image_async'); // called by serverside_crop.js

    // http://blog.tompawlak.org/calculate-checksum-hash-nodejs-javascript
    /*
    The checksum (aka hash sum) calculation is a one way process
    of mapping a large data set of variable length (e.g. message, file),
    to a smaller data set of a fixed length (hash).
    The length depends on a hashing algorithm.
    */
    function checksum (str, algorithm, encoding) {
        return crypto
            .createHash(algorithm || 'md5')
            .update(str, 'utf8') // this seems to expect a string, hence JSON.stringify()
            .digest(encoding || 'hex');
    }

    // If the source directory exists
    IsThere(json_directory, function (exists) {
        if (exists) {

            var json_files = json_directory + '*.json'; // glob
            var files_changed = false;

            // get all json files in the directory
            glob(json_files, function (er, files) {

                // if no matching files are found, exit
                if (er) throw er;

                // loop over the json files
                files.forEach(function(new_file) {

                    var hashed_extension = '.md5.json';

                    var a_hashed_file = new_file.match(hashed_extension);

                    // exclude hashed files
                    if ( ! a_hashed_file ) {

                        // read the data file
                        // data is in this format
                        /*
                        {
                            "design": "13-OUT3571-search-results.jpg",
                            "crop": "0,2095,872,123"
                        },
                        */
                        jf.readFile(new_file, function(err, data) {

                            // if there was a problem, exit
                            if (err) {
                                throw err;
                            }

                            // If it doesn't, create a version and perform the crop
                            var new_file_no_path = new_file.substr( new_file.lastIndexOf('/') + 1 );
                            var new_file_str = JSON.stringify(data);
                            var new_file_hash = checksum(new_file_str);
                            var hashed_filename = new_file.replace('.json', '__' + new_file_hash + hashed_extension);
                            var file_changed = false;

                            // if a hashed_filename exists
                            IsThere(hashed_filename, function (exists) {
                                if (exists) {

                                    // read the hashed_filename
                                    jf.readFile(hashed_filename, function(err, data) {

                                        // if there was a problem, exit
                                        if (err) {
                                            throw err;
                                        }

                                        // hash the contents of the hashed_filename
                                        var hashed_filename_str = JSON.stringify(data);
                                        var hashed_filename_hash = checksum(hashed_filename_str);

                                        // Compare the two hashes
                                        if ( hashed_filename_hash !== new_file_hash ) {

                                            // If there is a change

                                            // save a new hashed file
                                            console.log('Changed detected in', new_file_no_path);
                                            fs.writeFileSync( hashed_filename, new_file_str );

                                            // then set a flag to create the crop
                                            file_changed = true;
                                            files_changed = true;

                                            // TODO: remove the old hashed_file and matching crop
                                        }
                                    });
                                }
                                // if a saved copy doesn't exist for this version of the file
                                else {
                                    // save a new hashed file
                                    console.log('Changed detected in', new_file_no_path);
                                    fs.writeFileSync( hashed_filename, new_file_str );

                                    // then set a flag to create the crop
                                    file_changed = true;
                                    files_changed = true;
                                }

                                if ( regenerate_all_images === 'true' ) {
                                    console.log('Changed forced in', new_file_no_path);
                                    fs.writeFileSync( hashed_filename, new_file_str );
                                    // then set a flag to create the crop
                                    file_changed = true;
                                    files_changed = true;
                                }

                                // if the file was changed, perform the crop
                                if ( file_changed ) {

                                    // the data is in JSON format
                                    var obj = data;

                                    // create an object which can be passed to the cropping function
                                    var crop_obj = {};

                                    // the crop key contains crop points in a comma separated list
                                    // create an array from this list
                                    // and convert each item into an integer
                                    var crop_array = obj.crop.split(',');
                                    crop_obj.crop_w = parseInt(crop_array[0], 10);
                                    crop_obj.crop_h = parseInt(crop_array[1], 10);
                                    crop_obj.crop_x = parseInt(crop_array[2], 10); // ideally this would be optional, but the 'center' option in lwip affects both x and y
                                    crop_obj.crop_y = parseInt(crop_array[3], 10);

                                    // the design key contains a filename (JPG format)
                                    crop_obj.design_file = obj.design;

                                    // LWIP needs to know the image file type
                                    crop_obj.image_ext = crop_obj.design_file.substr( crop_obj.design_file.lastIndexOf('.') + 1 );

                                    // generate a unique filename for the cropped image

                                    // TODO: explain this better
                                    // we include the filename extension as we can't strip it out of the styleguide, without adding an otherwise redundant helper
                                    // so the final filename will have two extensions: 13-OUT3571-search-results.jpg.crop@872,123,0,2095.jpg
                                    var design_file_noext = crop_obj.design_file; // d.design_file.split('.jpg')[0];
                                    crop_obj.design_file_noext_cropped = design_file_noext + '.crop@' + crop_obj.crop_w + ',' + crop_obj.crop_h + ',' + crop_obj.crop_x + ',' + crop_obj.crop_y;

                                    // call a self-executing function, with:
                                    // - the data from the current json object
                                    // - the image source directory (a global)
                                    // - the image destination directory (a global)

                                    // this immediately executing function is passed its arguments when it is called
                                    ( function(image_crop_data) {

                                        //console.log("%j", image_crop_data); // http://stackoverflow.com/a/7428313

                                        var image_full = image_src_dir + image_crop_data.design_file;
                                        var image_full_dest = image_dest_dir + image_crop_data.design_file;
                                        var image_full_no_path = image_full.substr( image_full.lastIndexOf('/') + 1 );

                                        // copy the uncropped image to the destination directory
                                        console.log('Copying:', image_full_no_path);
                                        fs.writeFileSync( image_full_dest, fs.readFileSync( image_full ) );

                                        // generate a filepath and filename for the cropped image
                                        var image_crop = image_dest_dir + image_crop_data.design_file_noext_cropped + '.' + image_crop_data.image_ext;

                                        // crop the copied image
                                        // this runs asynchronously, ie when it is ready to do so
                                        serverside_crop_image_async(image_full_dest, image_crop_data, image_crop_data.image_ext, image_crop);

                                    } )(crop_obj);
                                }
                            });
                        });
                    }
                });
            });

            // TODO: this message is being output regardless of what happens
            //if ( ! files_changed ) {
            //    console.log('No changes detected, no crops required');
            //}
        }
    });
};

// Export the constructor from this module
module.exports = serverside_crop;
