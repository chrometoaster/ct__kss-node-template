// JSHINT:
/*globals require, module */

/** @function
 * @name serverside_crop_image_async
 * @requires FileQueue
 * @requires jsonfile
 * @requires lwip
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Save the cropped image, save a settings object for front-end 'cropping' in Raphael
 *
 * @param {String} image_full - The path to the full page design image
 * @param {String} image_crop_data - A crop data object
 * @param {String} image_ext - The type of image, as required by LWIP
 * @param {String} image_crop - The cropped image
 */

// constructor
var serverside_crop_image_async = function(image_full, image_crop_data, image_ext, image_crop) {

    // DEPENDENCIES

    // FileQueue - avoid opening more than allowed number of files at once - prevent reaching system limits
    // supports: readFile, writeFile, readdir, stat, exists
    var FileQueue = require('filequeue');
    var fq = new FileQueue(100);

    // Easily read/write JSON files.
    var jf = require('jsonfile');

    // Light Weight Image Processor for NodeJS
    var lwip = require('lwip');

    // GLOBALS

    var raphael_dest_dir = require('../app').raphael_dest_dir;
    var clientside_crop_options = require('./clientside_crop_options'); // called by serverside_crop_image_async.js

    fq.readFile(image_full, function(err, buffer) {

        var image_full_no_path = image_full.substr( image_full.lastIndexOf('/') + 1 );

        console.log( 'Reading:', image_full_no_path );

        if (err) throw err;

        lwip.open(buffer, image_ext, function(err, image){

            if (err) throw err;

            var _cropOpt = {
                left: image_crop_data.crop_x,
                top: image_crop_data.crop_y,
                // https://github.com/EyalAr/lwip/issues/134#issuecomment-105421970
                // crop coordinates are inclusive.
                // img.crop(0,0,1,1) will yield an image of 2X2 pixels,
                // starting from pixel (0,0) up to and including pixel (1,1).
                // so we adjust out width and height by 1px to allow for the zero pixels
                right: (image_crop_data.crop_x + image_crop_data.crop_w - 1),
                bottom: (image_crop_data.crop_y + image_crop_data.crop_h - 1)
            };

            var image_crop_no_path = image_crop.substr( image_crop.lastIndexOf('/') + 1 );

            image.crop(_cropOpt.left, _cropOpt.top, _cropOpt.right, _cropOpt.bottom, function(err, crpdImg) {
                if (err) throw err;
                crpdImg.writeFile(image_crop, function(err) {
                    if (err) throw err;
                    console.log( 'Writing:', image_crop_no_path );
                });
            });

            var clientside_crop_json = clientside_crop_options(image, image_crop_data, image_full);
            var filename = ( raphael_dest_dir + clientside_crop_json.id + '.json');
            var filename_no_path = filename.substr( filename.lastIndexOf('/') + 1 );

            jf.writeFile(filename, clientside_crop_json);

            console.log( 'Writing:', filename_no_path );

        });

    });
};

// Export the constructor from this module
module.exports = serverside_crop_image_async;
