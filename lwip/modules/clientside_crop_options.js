// JSHINT:
/*globals require, module */

/** @function
 * @name clientside_crop_options
 * @requires path
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Output JSON data storing values for client-side crop context previewing
 * @description
Create a preview showing the entire design with a box around the component.
This uses the client-side library Raphael, so we output the results to a JS file which is included in the page.
 *
 * @param {String} image - A full page design image as currently being processed by LWIP
 * @param {String} image_crop_data - A crop data object
 * @param {String} image_full - The path to the full page design image
 *
 * @returns {Object} clientside_crop_data for client-side processing
 */

// constructor
var clientside_crop_options = function(image, image_crop_data, image_full) {

    // DEPENDENCIES

    // This module contains utilities for handling and transforming file paths. Almost all these methods perform only string transformations. The file system is not consulted to check whether paths are valid.
    var path = require('path');

    // GLOBALS

    var styleguide_dir = require('../app').styleguide_dir;

    // Create a preview showing the entire design with a box around the component
    // This uses the client-side library Raphael
    // So we'll output the results to a JS file which is included in the page

    // vars passed in
    var _image = image;
    var _image_crop_data = image_crop_data;
    var _image_full = image_full;

    // local vars
    var raphael_img_w = _image.width();
    var raphael_img_h = _image.height();

    if ( raphael_img_w && raphael_img_h ) {

        var clientside_crop_data = {
            "id": 'ct_crop_context__' + _image_crop_data.design_file_noext_cropped.replace(/\./g, "_").replace(/-/g, "_").replace(/crop@/g, "").replace(/,/g, "_"),
            "img": {
                "src": path.relative(styleguide_dir, _image_full), // the path to the design image to display as a thumbnail
                "w": parseInt( raphael_img_w/6, 10), // the width to display the thumbnail at
                "h": parseInt( raphael_img_h/6, 10)  // the height to display the thumbnail at
            },
            "box": {
                "w": parseInt( _image_crop_data.crop_w/6, 10), // the pixel width of the crop box
                "h": parseInt( _image_crop_data.crop_h/6, 10), // the pixel height of the crop box
                "x": parseInt( _image_crop_data.crop_x/6, 10), // the horizontal pixel registration point of the crop box
                "y": parseInt( _image_crop_data.crop_y/6, 10)  // the vertical pixel registration point of the crop box
            }
        };

        //console.log(output);

        return clientside_crop_data;
    }
};

// Export the constructor from this module
module.exports = clientside_crop_options;
