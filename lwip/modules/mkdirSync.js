// JSHINT:
/*globals require, module */

/** @function
 * @name mkdirSync
 * @requires fs
 *
 * @description
http://stackoverflow.com/questions/13696148/node-js-create-folder-or-use-existing
 *
 * @param {String} path - Path to create
 */

// constructor
var mkdirSync = function (path) {

    // DEPENDENCIES

    // File System - File I/O is provided by simple wrappers around standard POSIX functions
    var fs = require('fs');

    try {
        fs.mkdirSync(path);
    } catch(e) {
        if ( e.code != 'EEXIST' ) throw e;
    }
};

// Export the constructor from this module
module.exports = mkdirSync;
