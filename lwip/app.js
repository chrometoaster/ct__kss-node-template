// JSHINT:
/*globals require,process,exports */

/**
 * @file lwip/app.js - Node.js script to crop styleguide images
 * @copyright Chrometoaster 2015
 * @author dan.smith@chrometoaster.com
 */

// MODULES
// http://blog.modulus.io/absolute-beginners-guide-to-nodejs - "Code Organization"

var serverside_crop = require('./modules/serverside_crop'); // called below
var mkdirSync = require('./modules/mkdirSync'); // called below

// VARIABLES

// node lwip/app.js --arg1="val" --arg2="val" --arg3="val"
// these are passed in as flags from subgrunt
// process.argv[0] // node
// process.argv[1] // path/to/lwip/app.js
// process.argv[2] // regenerate_all_images
// process.argv[3] // styleguide_directory
// process.argv[4] // image_directory

var regenerate_all_images;
if ( typeof process.argv[2] !== 'undefined' ) {
    regenerate_all_images = process.argv[2].split('=')[1].replace(/\"/g,'');
}
else {
    throw ("regenerate_all_images not defined");
}
exports.regenerate_all_images = regenerate_all_images;

var styleguide_dir;
if ( typeof process.argv[3] !== 'undefined' ) {
    styleguide_dir = process.argv[3].split('=')[1].replace(/\"/g,'');
}
else {
    throw ("styleguide_dir not defined");
}
exports.styleguide_dir = styleguide_dir;

var image_src_dir;
if ( typeof process.argv[4] !== 'undefined' ) {
    image_src_dir = process.argv[4].split('=')[1].replace(/\"/g,'') + "/";
}
else {
    throw ("image_src_dir not defined");
}
exports.image_src_dir = image_src_dir;

// GLOBALS
// http://www.hacksparrow.com/global-variables-in-node-js.html

var image_dest_dir = styleguide_dir + "/images/"; // TODO: copy image here before cropping
exports.image_dest_dir = image_dest_dir;

var raphael_dest_dir = styleguide_dir + "/raphael/"; // TODO: copy image here before cropping
exports.raphael_dest_dir = raphael_dest_dir;

// MODULE FUNCTION CALLS

// prevent serverside_crop from failing due to the destination folder not existing yet
mkdirSync(image_dest_dir);
mkdirSync(raphael_dest_dir);

serverside_crop( styleguide_dir + "/tmp/styleguide_crops/");