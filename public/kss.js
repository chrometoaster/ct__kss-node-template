/**
 * @file KSS Template UI
 * @author kneath
 * @author chrometoaster.com
 */

// Local settings for JSHint

/*jshint browser:true, jquery:true, strict:true, devel:false */

/*globals KSS_UI_SHOW_ALL_NAVIGATION_LEVELS:true, KSS_UI_SHOW_GRID_SHADING:true, KSS_UI_SHOW_DESIGNS:true, KSS_UI_SHOW_DESIGN_AT_BREAKPOINT:true, KSS_UI_SHOW_WIREFRAMES:true, KSS_UI_KEEP_TARGET_IN_VIEWPORT:true, ct__debouncer, CSSRule, prettyPrint, Raphael */

/** @function
 * @name KssStateGenerator
 *
 * @author kneath
 *
 * @summary This class scans your stylesheets for pseudo classes, then inserts a new CSS rule with the same properties, but named 'psuedo-class-{{name}}'
 */

(function() {

  "use strict";

  var KssStateGenerator;

  KssStateGenerator = (function() {

    var pseudo_selectors;

    pseudo_selectors = ['hover', 'enabled', 'disabled', 'active', 'visited', 'focus', 'target', 'checked', 'empty', 'first-of-type', 'last-of-type', 'first-child', 'last-child'];

    function KssStateGenerator() {
      var idx, idxs, pseudos, replaceRule, rule, stylesheet, _i, _len, _len2, _ref, _ref2;
      pseudos = new RegExp("(\\:" + (pseudo_selectors.join('|\\:')) + ")", "g");
      try {
        _ref = document.styleSheets;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          stylesheet = _ref[_i];
          if (stylesheet.href && stylesheet.href.indexOf(document.domain) >= 0) {
            idxs = [];
            _ref2 = stylesheet.cssRules;
            for (idx = 0, _len2 = _ref2.length; idx < _len2; idx++) {
              rule = _ref2[idx];
              if ((rule.type === CSSRule.STYLE_RULE) && pseudos.test(rule.selectorText)) {
                replaceRule = function(matched, stuff) {
                  return matched.replace(/\:/g, '.pseudo-class-');
                };
                this.insertRule(rule.cssText.replace(pseudos, replaceRule));
              }
              pseudos.lastIndex = 0;
            }
          }
        }
      } catch (_error) {}
    }

    KssStateGenerator.prototype.insertRule = function(rule) {
      var headEl, styleEl;
      headEl = document.getElementsByTagName('head')[0];
      styleEl = document.createElement('style');
      styleEl.type = 'text/css';
      if (styleEl.styleSheet) {
        styleEl.styleSheet.cssText = rule;
      } else {
        styleEl.appendChild(document.createTextNode(rule));
      }
      return headEl.appendChild(styleEl);
    };

    return KssStateGenerator;

  })();

  new KssStateGenerator;

}).call(this);

/** @global
 * @name KSS_UI
 * @summary Exposes the KSS_UI namespace for console debugging
 */

    var KSS_UI;

/** @namespace
 * @name KSS_UI
 */

    KSS_UI = {

/** @method
 * @name generate_toggle_control
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @summary Injects a checkbox which controls an aspect of the styleguide UI
 *
 * @param {String} target_selector - the CSS selector of the element to toggle
 * @param {String} alternate_control_selector - the CSS selector of another element than can control the toggle
 * @param {String} toggle_label - the text label for the checkbox
 * @param {String} target_selector - an optional prefix for the generated toggle data-attribute, useful for custom CSS styling
 * @param {Method} KSS_UI_callback_method_name - a KSS_UI method to call whenever the toggle is clicked
 *
 * @todo: Support linked/dependent toggles
 */

      generate_toggle_control: function(target_selector, alternate_control_selector, toggle_label, toggle_attr_suffix, KSS_UI_callback_method_name, enable_on_load) {

        "use strict";

        var selector = target_selector;
        var $tools = jQuery('#kss-nav-tools');

        if ( ! jQuery(selector).length ) {
          return;
        }

        if ( ! $tools.length ) {
          return;
        }

        var $target = jQuery( selector );
        var $alternate_control;

        var toggle_id = 'kss-nav-tools_' + target_selector.replace('[','').replace(']','').replace('#','').replace('.','').replace('class^="','').replace('"','');

        // if a toggle already targets the target_selector, generate a unique ID
        var $toggles_with_same_id = jQuery('[id="' + toggle_id + '"]');
        if ( $toggles_with_same_id.length ) {
          toggle_id += '_' + ( $toggles_with_same_id.length + 1 );
        }

        var toggle_attr = 'data-toggle-show';

        if ( toggle_attr_suffix ) {
          toggle_attr += toggle_attr_suffix;
        }

        $target.attr(toggle_attr, 'false');

        var toggle_html = '<label><input type="checkbox" id="' + toggle_id + '" />' + toggle_label + '</label>';

        $tools.append(toggle_html);

        jQuery('#' + toggle_id).click( function() {
          $target.attr(toggle_attr, jQuery(this).is(':checked') );

          if ( ( typeof KSS_UI_callback_method_name !== 'undefined' ) && ( typeof KSS_UI_callback_method_name !== 'boolean' ) ) {
            window.setTimeout( function() {
              KSS_UI[KSS_UI_callback_method_name]();
            }, 500);
          }
        });

        if ( alternate_control_selector ) {
          $alternate_control = jQuery(alternate_control_selector);

          // also allow one-time manual control by user
          // TODO: add logic to collapse other open items
          $alternate_control.click( function() {
            $alternate_control.removeClass('is-active');
            jQuery(this).addClass('is-active');
            //jQuery('#' + toggle_id).click(); // affects all, we only want to affect this one:
            jQuery(this).parents(target_selector).eq(0).attr(toggle_attr, ! jQuery('#' + toggle_id).is(':checked') );
          });
        }

        if ( enable_on_load ) {
          jQuery('#' + toggle_id).click();
        }

      },

/** @method
 * @name highlight_active_nav
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @summary Highlights the active navigation item
 *
 * @todo Target anchor doesn't highlight in pink on load (CSS)
 */

      highlight_active_nav: function() {

        "use strict";

        var $nav_links = jQuery('.kss-nav__menu-item > a');
        var $heading_links = jQuery('.kss-title__permalink');
        var $all_links = $nav_links.add( $heading_links );

        // remove existing highlighting
        $nav_links.removeClass('is-active');

        var $active_item = $nav_links.filter('[href$="' + location.hash + '"]');

        // collapse any non active items
        var $non_active_parents = $('[data-toggle-show]').not('.is-active');
        $non_active_parents.attr('data-toggle-show', false);

        // add pink highlighting
        $active_item.addClass('is-active');

        // find the new active item's parent, no matter how deeply nested the active item
        // expand the parent to show the active item
        var $active_parents = $active_item.parents('[data-submenu-parent]');

        if ( $active_parents.length ) { // false if item is so deep that the nav can't display it
          $active_parents.attr('data-toggle-show', true);
        }

        $all_links.click( function() {

          setTimeout( function() {
            KSS_UI.highlight_active_nav();
          }, 100 );

        });
      },

/** @method
 * @name setup_search_form
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @summary Injects and sets up the Tipue Drop plugin
 */

      setup_search_form: function() {

        "use strict";

        // inject the search form as it's only usable when JS is enabled
        $('#tipue_drop_content').before('<form><input type="text" id="tipue_drop_input" autocomplete="off" placeholder="Search Styleguide" /></form>');

        // set up search autosuggest
        $('#tipue_drop_input').tipuedrop({
          'contentLocation': 'tipue_drop_data.json', // if mode.json; note: requires styleguide to be viewed via a webserver
          'maxWidth': 350, // 250|0, note that 40px is subtracted from this
          'mode': 'json', // static|json, json is faster, if static: public/tipuedrop_content.js must be attached to the page and define var tipuedrop = {}
          'newWindow': false, //  false|true
          'show': 999, // 3|n, a higher number is useful as it's not obvious if some results are being artificially suppressed
          'speed': 300 // 300|n
         });
      },

/** @method
 * @name setup_navigation
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @param {String} selector - the jQuery selector of the side menu
 *
 * @summary Enhance the side navigation
 */

      setup_navigation: function(selector) {

        "use strict";

        // Nest level 3 items within level 2 items, to improve styling and semantics
        // move subnavs into the parent item
        var $nav_l2 = $(selector + ' [data-submenu-parent]');

        $nav_l2.each( function(i, item) {

          var $nav_l3 = $(item).nextUntil('[data-submenu-parent]');

          var $nav_l3_list = $('<ul></ul>').append($nav_l3);

          $(item).append($nav_l3_list);

        });

        var $nav_l3 = $(selector + ' [data-submenu]');

        $nav_l3.each( function(i, item) {

          var $nav_l4 = $(item).nextUntil('[data-submenu]');

          var $nav_l4_list = $('<ul></ul>').append($nav_l4);

          $(item).append($nav_l4_list);

        });

      },

/** @method
 * @name setup_html_links
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @summary Enhance view-html links so that they Ajax the target page into the styleguide
 */

      setup_html_links: function() {

        "use strict";

        // load markup when 'Show HTML' is clicked
        // or toggle the visibility of the markup that is already there
        var $markup_links = $('.kss-markup-link');

        $markup_links.each( function(i, item) {

          var $link = $(item);
          var $markup_target = $( $link.attr('data-markup-target') );

          $link.click( function(e) {

            e.preventDefault();

            // if markup not loaded
            if ( $markup_target.attr('data-markup-loaded' ) === 'false' ) {

              // Show loading message
              $markup_target.html('<p class="ct-kss-loading">Loading HTML..</p>');

              // load markup
              $.ajax({
                url: $(this).attr('href')
              })
              .done(function(data) {

                $link.text('Hide HTML');

                $markup_target
                  .slideUp(0)
                  .empty() // remove loader
                  .append(data)
                  .attr('data-markup-loaded', 'true')
                  .attr('data-markup-hidden', 'false')
                  .slideDown();

                $link.text('Hide HTML');

                // style code
                prettyPrint();
              });

            }
            // if markup loaded
            else {
              // if hidden, show
              if ( $markup_target.attr('data-markup-hidden') === 'true' ) {

                $markup_target
                  .attr('data-markup-hidden', 'false')
                  .slideDown();

                $link.text('Hide HTML');
              }
              // if shown, hide
              else if ( $markup_target.attr('data-markup-hidden') === 'false' ) {

                $markup_target.attr('data-markup-hidden', true);

                // only slide up if it was previously slid down
                if ( $markup_target.is(':visible') ) {
                  $markup_target.slideUp();
                }

                $link.text('Show HTML');
              }
            }

          });

        });

      },

/** @method
 * @name show_target_in_viewport
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @summary Scrolls the current hash/target into the viewport, which is necessary on resize or after UI elements have been toggled on or off
 * @description
https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollIntoView
 */

      show_target_in_viewport: function() {

        "use strict";

        if ( jQuery('.kss-main').attr('data-toggle-show_target-on-resize') !== 'true' ) {
          return;
        }

        if ( location.hash !== '' ) {

          var $target_anchor = jQuery(location.hash);

          if ( $target_anchor.length ) {
            $target_anchor.get(0).scrollIntoView();
          }

          // nav item which links to target anchor
          // and is in a separate scrollable div
          var $active_nav_item = jQuery('.kss-nav a[href$="' + location.hash + '"]');

          if ( $active_nav_item.length ) {
            $active_nav_item.get(0).scrollIntoView();
          }
        }
      },

/** @method
 * @name enhance_context_links
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @summary Adds listeners to inject a thumbnail of the design with a box around the design crop
 *
 * @todo Add close button to generated overlay
 * @todo Make the preview automatically update when you stop scrolling
 */

      enhance_context_links: function() {

        "use strict";

        var target_id = 'crop_context';
        var target_closer_id = 'crop_context_closer';
        var target_liner_id = 'crop_context_liner';
        var $target = jQuery('<div id="' + target_id + '" class="kss-style"><input type="button" id="' + target_closer_id + '" class="kss-markup-action" value="Hide crop context" /><a href="" target="_blank" style="display:block;" id="' + target_liner_id + '" title="Open this image in a new window/tab. "></a></div>');
        jQuery('body').append( $target );
        var $target_liner = jQuery('#' + target_liner_id);

        $target.hide();

        jQuery('#' + target_closer_id).on( 'click', function() {
          $target_liner.empty();
          $target.hide();
        });

        var $links = jQuery('[data-crop-context]');

        $links.click( function(e) {

          e.preventDefault();

          $target_liner.empty();
          $target.show();

          $target_liner.attr('href', $(this).attr('href'));

          var $this = jQuery(this);
          var id = $this.attr('data-crop-context');
          var crop_data_file = 'raphael/' + id + '.json';

          $.getJSON( crop_data_file, function( data ) {

            // canvas dom id and size
            var r = Raphael( target_liner_id, data.img.w, data.img.h);

            // image size: x,y,w,h
            r.image(data.img.src, 0, 0, data.img.w, data.img.h);

            // 'crop' box: x,y,w,h
            r.rect(data.box.x, data.box.y, data.box.w, data.box.h).attr({
                stroke: "#f09"
            });

          });


        });


        // Shown with designs
        //if ( ! jQuery('#kss-nav-tools_data-crop-context').length ) {
        //  KSS_UI.generate_toggle_control( '[data-crop-context]', false, 'Show crop context', false );
        //}

      },

/** @method
 * @name js_enhance
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @summary Displays the status of JavaScript enhancements on a markup example
 *
 * @param {String} js_enhanced_id - the ID of the example's status element
 *
 * @example
 * ct-nojs: true
 */

      js_enhance: function(js_enhanced_id) {

        "use strict";

        var $js_status = jQuery('#' + js_enhanced_id);

        if ( !$js_status.length ) {
          return;
        }

        $js_status.attr('data-kss-js-enhanced', true);
      },

/** @method
 * @name emulate_noscript
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @summary Displays a styleguide example in its JavaScript-disabled view
 *
 * @param {String} kss_example_markup_id - the example's KSS index
 *
 * @example
 * ct-nojs: true
 */

      emulate_noscript: function(kss_example_markup_id) {

        "use strict";

        // modules.forms.warning -> kssref-modules_forms_warning
        var $wrapper = jQuery('#' + kss_example_markup_id).next('.ct-kss-modifiers').eq(0).find('.kss-modifier__example:not(".ct-kss-notes")').eq(0),
           wrapper_html = $wrapper.get(0).innerHTML,
           // note that we need to replace the tag brackets, which I presume are encoded by the browser to hide the noscript content
           script_html = wrapper_html.replace(/<noscript>/g, '<div data-tag-noscript>').replace(/<\/noscript>/g, '</div>').replace(/&lt;/g, '<').replace(/&gt;/g, '>');

        // add the hierarchical styling class
        $wrapper.addClass('no-js');

        // <noscript></noscript> -> <div data-tag-noscript></div>
        $wrapper
          .empty()
          .html( jQuery.trim(script_html) );
      },

/** @method
 * @name draggable_designs
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @summary Makes designs draggable, for simplified design matching against the build
 */

      draggable_designs: function() {

        "use strict";

        /*

        // http://stackoverflow.com/questions/6230834/html5-drag-and-drop-anywhere-on-the-screen
        // http://jsfiddle.net/robertc/kKuqH/
        // http://stackoverflow.com/questions/13949422/jquery-html-5-dragstart-ondragstart-functione-e-datatransfer-setdata

        jQuery('[draggable]')
          .attr('data-dragging', false)
          .on('dragstart', function(e) {
            var style = window.getComputedStyle(e.target, null);
            console.log(style);
            e.originalEvent.dataTransfer.setData("text/plain", (parseInt(style.getPropertyValue("left"),10) - e.originalEvent.clientX) + ',' + (parseInt(style.getPropertyValue("top"),10) - e.originalEvent.clientY));

            $(this).attr('data-dragging', true);
          });
          //.on('dragend', function(e) {
          //  $(this).attr('data-dragging', false);
          //});

        jQuery('body')
          .on('dragover', function(e) {
            e.preventDefault();
            return false;
          })
          .on('drop', function(e) {
            var offset = e.originalEvent.dataTransfer.getData("text/plain").split(',');

            console.log(offset);

            jQuery('[data-dragging="true"]')
              .css({
                left: (e.originalEvent.clientX + parseInt(offset[0],10)) + 'px',
                top: (e.originalEvent.clientY + parseInt(offset[1],10)) + 'px'
              })
              .attr('data-dragging', false);

            e.preventDefault();
            return false;
          });

        */

        jQuery('[draggable]')
          .draggable({
            start: function() {
              $(this)
                .css('opacity', 0.5)
                .attr('title', 'double click to toggle full opacity');

              $(this).parent('.kss-modifier__ct-kss-design').attr('data-dragged-content', true);
            }
          })
          .on( 'dblclick', function() {

            var $this = jQuery(this);

            if ( $this.css('opacity') == 1 ) {
              $this.css('opacity', 0.5);
            }
            else {
              $this.css('opacity', 1);
            }

          });

      },

/** @method
 * @name setup_description_code_links
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @param {Array} code_extensions - extensions which should open in a new window
 *
 * @summary Opens links to code demos in a new window
 */

      setup_description_code_links: function(code_extensions) {

        "use strict";

        if ( typeof code_extensions === 'undefined' ) {
          return;
        }

        var code_extensions_arr = [];
        var code_extensions_str = '';
        var $code_links = $();

        $.each( code_extensions, function(i, item) {
          code_extensions_arr.push('[href$=".' + item + '"]');
        });

        code_extensions_str = code_extensions_arr.join(", ");

        $code_links = $('.kss-description a').filter(code_extensions_str);

        $code_links.each( function(i, item) {
          var $item = $(item);
          var item_text = $item.text();

          $item
            .attr('target', '_blank')
            .attr('title', item_text + ' (opens in a new tab/window)' );
        });
      },

/** @method
 * @name always_show_target_in_viewport
 * @memberof KSS_UI
 * @author dan.smith@chrometoaster.com
 *
 * @summary Update the UI when the viewport is resized
 */

      always_show_target_in_viewport: function() {

        "use strict";

        ct__debouncer(jQuery,'smartresize', 'resize', 100);

        $(window).smartresize( function(e) {
            // this is also fired by iphone 5 / iOS 7 on orientationchange
            KSS_UI.show_target_in_viewport();
        });

        KSS_UI.show_target_in_viewport();
      },

/** @method
 * @name init
 * @memberof KSS_UI
 *
 * @author dan.smith@chrometoaster.com
 *
 * @summary Calls the methods of KSS_UI, in the most appropriate order
 */

      init: function() {

        "use strict";

        // style JS code blocks
        prettyPrint();

        this.setup_search_form();

        this.setup_navigation('.kss-nav__menu');

        this.setup_html_links();

        this.generate_toggle_control( '[data-submenu-parent]', '[data-submenu-parent] > a', 'Show all navigation levels', false, false, KSS_UI_SHOW_ALL_NAVIGATION_LEVELS );

        this.generate_toggle_control( '.kss-main', false, 'Show grid shading', '_l-debug', false, KSS_UI_SHOW_GRID_SHADING );

        this.generate_toggle_control( '[class^="kss-ct-design"]', false, 'Show designs', false, 'show_target_in_viewport', KSS_UI_SHOW_DESIGNS );

        // we use the [class^="kss-ct-design"] selector so that the toggle doesn't appear if there are no designs
        this.generate_toggle_control( '[class^="kss-ct-design"]', false, 'Show design @ breakpoint ', '_design_at_breakpoint', 'show_target_in_viewport', KSS_UI_SHOW_DESIGN_AT_BREAKPOINT );

        this.generate_toggle_control( '.kss-ct-wireframe', false, 'Show wireframes', false, 'show_target_in_viewport', KSS_UI_SHOW_WIREFRAMES );

        this.generate_toggle_control( '.kss-main', false, 'Keep target in viewport ', '_target-on-resize', 'always_show_target_in_viewport', KSS_UI_KEEP_TARGET_IN_VIEWPORT );

        this.enhance_context_links();

        this.draggable_designs();

        // Button is left enabled for hijacking by site script
        // eg for revalidation purposes
        //$('.kss-markup-actions > input').on('click', function() {
        //  $(this).attr('disabled', 'disabled');
        //});

        this.setup_description_code_links(['js', 'hbs']);

      }
    };

/** @function
 * @author dan.smith@chrometoaster.com
 * @summary Anonymous function which fires when the DOM has finished loading
 */

    if ( ( typeof jQuery === 'undefined' ) && ( typeof $ !== 'undefined' ) ) {
      jQuery = $;
    }

    if ( ( typeof $ === 'undefined' ) && ( typeof jQuery !== 'undefined' ) ) {
      $ = jQuery;
    }

    jQuery(document).ready( function($) {

      "use strict";

      // dependencies
      if ( typeof KSS_UI !== 'undefined' ) {
        KSS_UI.init();
      }

    });

/** @function
 * @author dan.smith@chrometoaster.com
 * @summary Anonymous function which fires when the window has finished loading
 */

    jQuery(window).load( function($) {

      "use strict";

      window.setTimeout( function() {
        KSS_UI.show_target_in_viewport();
        KSS_UI.highlight_active_nav();
      }, 100);

    });
